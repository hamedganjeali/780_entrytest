package ir.hamedganjeali.hafhashtadtest.ui.detail;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import ir.hamedganjeali.hafhashtadtest.R;
import ir.hamedganjeali.hafhashtadtest.model.Actor;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;


public class InfoFragment extends Fragment {

    private static final String KEY_ACTOR = "actor_key";

    private Actor mActor;

    View view;
    ProgressBar progressBar;
    private TextView ageValueTv,
            bornValueTv,
            fromValueTv,
            bioValueTv,
            noItemTv;


    public static InfoFragment newInstance(Actor actor) {
        InfoFragment fragment = new InfoFragment();
        Bundle args = new Bundle();
        args.putSerializable(KEY_ACTOR, actor);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        if (getArguments() != null) {
            mActor = (Actor) getArguments().getSerializable(KEY_ACTOR);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        if (view == null)  view = inflater.inflate(R.layout.fragment_info, container, false);


        init(view);
        setData();
        return view;
    }


    private void init(View view) {
        ageValueTv =  view.findViewById(R.id.age_value);
        bornValueTv =  view.findViewById(R.id.born_value);
        fromValueTv =  view.findViewById(R.id.from_value);
        bioValueTv =  view.findViewById(R.id.bio_value);
        progressBar =  view.findViewById(R.id.progress_circular);
        progressBar.setVisibility(View.VISIBLE);


    }

    private void setData() {
        if(mActor !=null){
            progressBar.setVisibility(View.INVISIBLE);
            ageValueTv.setText(getText(mActor.getAge()));
            bornValueTv.setText(getText(mActor.getBirthday()));
            fromValueTv.setText(getText(mActor.getPlaceOfBirth()));
            bioValueTv.setText(getText(mActor.getBiography()));
        }
//        else Log.e("LOG", "actor null: " );



    }

    private String getText(String s){
        if (s == null || s.length()<1){
            return  getString(R.string.no_item);
        }
        return s;
    }


}