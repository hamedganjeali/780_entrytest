package ir.hamedganjeali.hafhashtadtest.ui.adapters;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import ir.hamedganjeali.hafhashtadtest.model.ProductKnownFor;
import ir.hamedganjeali.hafhashtadtest.ui.detail.InfoFragment;
import ir.hamedganjeali.hafhashtadtest.ui.detail.KnownForFragment;
import ir.hamedganjeali.hafhashtadtest.model.Actor;

public class FragmentAdapter extends FragmentStateAdapter {
    private Actor actor;
    private  List<ProductKnownFor> products;

    public FragmentAdapter(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle,
                           Actor actor, List<ProductKnownFor> products) {
        super(fragmentManager, lifecycle);
        this.actor = actor;
        this.products = products;
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {

        switch (position)
        {
            case 1 :
                return KnownForFragment.newInstance(products);

        }

        return  InfoFragment.newInstance(actor);
    }

    @Override
    public int getItemCount() {
        return 2;
    }
}
