package ir.hamedganjeali.hafhashtadtest.ui.detail;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager2.widget.ViewPager2;
import ir.hamedganjeali.hafhashtadtest.Utils;
import ir.hamedganjeali.hafhashtadtest.R;
import ir.hamedganjeali.hafhashtadtest.model.ActorWork;
import ir.hamedganjeali.hafhashtadtest.model.ProductKnownFor;
import ir.hamedganjeali.hafhashtadtest.ui.ViewModelGlobal;
import ir.hamedganjeali.hafhashtadtest.ui.adapters.FragmentAdapter;
import ir.hamedganjeali.hafhashtadtest.model.Actor;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;
import com.squareup.picasso.Picasso;

import java.util.List;

public class DetailActivity extends AppCompatActivity {

    FragmentAdapter adapter;
    Actor actor;
    ActorWork actorWorkIntent;
    String actorSearchAddedUrl;
    ViewModelGlobal viewModel;
    Activity activity = this;

    TabLayout tabLayout;
    ViewPager2 pager2;
    ImageView imageView;
    TextView nameTv, careerTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        actorWorkIntent = (ActorWork) getIntent().getExtras().getSerializable(Utils.KEY_ACTOR_WORK_OBJ);
        if (actorWorkIntent != null) {
            init();
            // fetching data to show
            fetchActorAndShow(actorSearchAddedUrl, actorWorkIntent.getActorId().toString());
        } else {
            Utils.showConnectionDialog(activity);
            Toast.makeText(activity, "actorWorkIntent null", Toast.LENGTH_SHORT).show();

        }


    }


    // -----------------------      Methods     -----------------------//

    private void init() {
        // views
        tabLayout = findViewById(R.id.tab_layout);
        pager2 = findViewById(R.id.view_pager2);
        imageView = findViewById(R.id.image);
        nameTv = findViewById(R.id.tv_actor_name);
        careerTv = findViewById(R.id.tv_actor_etc);

        setUpTabPager();
        // init info of upper page
        InitInfoOfUpperPage(actorWorkIntent);
        // other inits
        actorSearchAddedUrl = Utils.PEOPLE_SEARCH_ADDED_URL + actorWorkIntent.getActorId() + "?" + Utils.API_KEY + "=" + Utils.API_KEY_VALUE;
        viewModel = new ViewModelGlobal(activity);
        if (!isNetworkAvailable()) Utils.isRoomDataNeeded = true;

    }


    //---------------       fetching info       ------------------//
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    private void fetchActorAndShow(String actorSearchAddedUrl, String actorId) {
        viewModel.getActorById(actorSearchAddedUrl, actorId, Utils.isRoomDataNeeded).observe(this, actor0 -> {
            actor = actor0;
            if (actor0 != null) {
                // show data
                //no need to re-download products. it has been passed through intent. it cannot be null.
                showDataOnPagers(actor, actorWorkIntent.getProductKnownFor());
            } else{
                Toast.makeText(activity, "actor null", Toast.LENGTH_SHORT).show();

                Utils.showConnectionDialog(activity);
            }
        });

    }

    private void InitInfoOfUpperPage(ActorWork actorWork) {
        String imageUrl = Utils.PHOTO_BASE_URL + actorWork.getProfilePath();
        imageView.setImageResource(R.drawable.avatar);
        if (imageUrl.length() > Utils.PHOTO_BASE_URL.length() + 1)   Picasso.get().load(imageUrl).into(imageView);
        nameTv.setText(actorWork.getName());
        careerTv.setText(actorWork.getKnownFor());
    }


    private void setUpTabPager() {


        tabLayout.addTab(tabLayout.newTab().setText("About"));
        tabLayout.addTab(tabLayout.newTab().setText("Known For"));

        // click on tabs
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                pager2.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        // swipe pages
        pager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                tabLayout.selectTab(tabLayout.getTabAt(position));
            }
        });
    }

    private void showDataOnPagers(Actor actor, List<ProductKnownFor> products) {
        FragmentManager fm = getSupportFragmentManager();
        adapter = new FragmentAdapter(fm, getLifecycle(), actor, products);
        pager2.setAdapter(adapter);
    }


}