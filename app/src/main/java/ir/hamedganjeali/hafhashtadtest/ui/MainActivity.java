package ir.hamedganjeali.hafhashtadtest.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import ir.hamedganjeali.hafhashtadtest.Utils;
import ir.hamedganjeali.hafhashtadtest.R;
import ir.hamedganjeali.hafhashtadtest.ui.adapters.MainAdapter;
import ir.hamedganjeali.hafhashtadtest.model.ActorWork;
import ir.hamedganjeali.hafhashtadtest.ui.detail.DetailActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    Activity activity;
    MainAdapter mainAdapter;
    ViewModelGlobal viewModel;

    List<ActorWork> allActorWorks;
    int totalPages = 1;
    int lastDownloadedPage = 0;  // the last one that we have already downloaded
    int currentPage = 1; // the we need to download
    boolean letCalling =true;

    RelativeLayout cover;
    RecyclerView recyclerView;
    private ProgressBar progressBarEndless;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();

        // fetching  data and show
        fetchMoreDataAndShow();

    }

    // ----------------------------      Methods     ----------------------------//

    private void init() {
        // views
        recyclerView = findViewById(R.id.rv);
        cover = findViewById(R.id.cover_main_rl);
        cover.setVisibility(View.VISIBLE);
        progressBarEndless = findViewById(R.id.endless_progressbar);
        progressBarEndless.setVisibility(View.GONE);
        //
        activity = this;
        allActorWorks = new ArrayList<>();
        viewModel = new ViewModelGlobal(activity);

        lastDownloadedPage = viewModel.getLastDownloadedPage(); // Once here (first) and then everytime calling fetchData
        Utils.isRoomDataNeeded = isNetworkAvailable() || (!isItNewDay() && currentPage < (lastDownloadedPage + 1));


        setRecyclerView(allActorWorks);
        if (isItNewDay()) viewModel.removeLastDownloadedPage();



        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                // if end of scroll & time to get more online:
                if (!recyclerView.canScrollVertically(1)) {

                    if (currentPage < totalPages) {
                        fetchMoreDataAndShow();
                        letCalling = true;
                    }
                }
            }

        });

    }

    //------------      call & network

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo == null || !activeNetworkInfo.isConnected();
    }


    private void fetchMoreDataAndShow() {
        lastDownloadedPage = viewModel.getLastDownloadedPage();
        String popularGetUrl = Utils.POPULAR_ACTORS_ADDED_URL + "?" + Utils.API_KEY + "=" + Utils.API_KEY_VALUE + "&page=" + currentPage;
        if (currentPage != 1) progressBarEndless.setVisibility(View.VISIBLE);

        viewModel.getAllActorsWorks(popularGetUrl, Utils.isRoomDataNeeded, currentPage).observe(this, actorWorks -> {
            progressBarEndless.setVisibility(View.GONE);
            cover.setVisibility(View.GONE);

            // inform that today we have downloaded once
            saveTodayCallTime();

            // handle null
            if (actorWorks == null || actorWorks.size() < 1) {
                cover.setVisibility(View.VISIBLE);
                Utils.showConnectionDialog(activity);
                return;
            }
            // data inserts to Room automatically
            // get offline if:
            // 1. user is offline (getting outdated info)
            // 2. if data has already been downloaded today
            if (Utils.isRoomDataNeeded) {

                for (ActorWork actorWork : actorWorks) {
                    // add Products to these ActorWorks manually as dara is receiving from Room
                    viewModel.getAllProductsByActorId(actorWork.getActorId().toString()).observe(this, actorWork::setProductKnownFor);
                }
                currentPage = actorWorks.get(actorWorks.size() - 1).getPage(); // this is last page. at the end we do currentPage++

            }

            if (letCalling){
                int oldCount = actorWorks.size();
                totalPages = actorWorks.get(0).getTotalPages();
                allActorWorks.addAll(actorWorks);
                // save last page, if larger thar previous one
                int newCount = allActorWorks.size();

                int thisPage = actorWorks.get(actorWorks.size() - 1).getPage();
                currentPage = thisPage + 1; //next current page
                if (thisPage > lastDownloadedPage) saveLastDownloadedPage(thisPage);
//            currentPage = lastDownloadedPage + 1;

                mainAdapter.notifyItemRangeInserted(oldCount, newCount);
                mainAdapter.notifyDataSetChanged();


                boolean b = isNetworkAvailable() || (!isItNewDay() && currentPage < (lastDownloadedPage + 1));
                Utils.isRoomDataNeeded = b;
                letCalling = false;
            }

        });
    }

    private void setRecyclerView(List<ActorWork> actorWorks) {
        mainAdapter = new MainAdapter(actorWorks, selectedActorWork -> {

            Intent i = new Intent(activity, DetailActivity.class);
            i.putExtra(Utils.KEY_ACTOR_WORK_OBJ, selectedActorWork);
            activity.startActivity(i);
        });

        int itemsInRow = 3;
        mainAdapter.setItemsInRow(3);
        recyclerView.setLayoutManager(new GridLayoutManager(activity, itemsInRow));
        recyclerView.setAdapter(mainAdapter);
    }


    //------------      checking if already downloaded today

    private boolean isItNewDay() {
        String lastCallStr = viewModel.getLastCallDay();
        String todayStr;

        Calendar today = Calendar.getInstance();
        today.add(Calendar.DAY_OF_WEEK, 0); // today
        todayStr = today.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.US);
        return !(todayStr.equals(lastCallStr));
    }

    private void saveTodayCallTime() {
        String lastCallStr;
        Calendar lastCall = Calendar.getInstance();
        lastCall.add(Calendar.DAY_OF_WEEK, 0); // today
        lastCallStr = lastCall.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.US);
        viewModel.saveLastDownloadedPage(lastCallStr); //save in shared preferences
    }

    private void saveLastDownloadedPage(int lastDownloaded) {
        lastDownloadedPage = lastDownloaded;
        viewModel.saveLastDownloadedPage(lastDownloaded); //save in shared preferences
    }


}