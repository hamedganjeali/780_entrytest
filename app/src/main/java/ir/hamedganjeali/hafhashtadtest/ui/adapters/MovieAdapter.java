package ir.hamedganjeali.hafhashtadtest.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import ir.hamedganjeali.hafhashtadtest.R;
import ir.hamedganjeali.hafhashtadtest.Utils;
import ir.hamedganjeali.hafhashtadtest.model.ProductKnownFor;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.ActorViewHolder> {
    String picUrl ;
    List<ProductKnownFor> movies;

    public MovieAdapter(List<ProductKnownFor> movies) {
        this.movies = movies;
    }

    @NonNull
    @Override
    public ActorViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_movie, viewGroup, false);
        return new ActorViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ActorViewHolder holder, int i) {
        ProductKnownFor movie = movies.get(i);

        String name = movie.getTitle();
        if (name == null || name.trim().equals("")) {
            holder.txtTitle.setText(R.string.no_name);
        } else  holder.txtTitle.setText(movie.getTitle());


        picUrl = Utils.PHOTO_BASE_URL + movie.getPosterPath();

        holder.imageView.setBackgroundResource(R.drawable.avatar);
        if (picUrl != null && picUrl.trim().length()>2)  Picasso.get().load(picUrl).into(holder.imageView);

    }

    @Override
    public int getItemCount() {
        return movies.size();
    }


    public class ActorViewHolder extends RecyclerView.ViewHolder {
        TextView txtTitle;
        ImageView imageView;

        public ActorViewHolder(@NonNull View itemView) {
            super(itemView);
            txtTitle = itemView.findViewById(R.id.title);
            imageView = itemView.findViewById(R.id.image);
        }
    }



}
