package ir.hamedganjeali.hafhashtadtest.ui.detail;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import ir.hamedganjeali.hafhashtadtest.R;
import ir.hamedganjeali.hafhashtadtest.Utils;
import ir.hamedganjeali.hafhashtadtest.ui.ViewModelGlobal;
import ir.hamedganjeali.hafhashtadtest.ui.adapters.MovieAdapter;
import ir.hamedganjeali.hafhashtadtest.model.ProductKnownFor;


public class KnownForFragment extends Fragment {

    private static final String KEY_PRODUCTS = "products_key";

    private List<ProductKnownFor> products;
    MovieAdapter adapter;
    View view;
    RecyclerView recyclerView;
    ViewModelGlobal viewModel;
    Activity activity;
    ProgressBar progressBar;


    public static KnownForFragment newInstance(List<ProductKnownFor> products) {
        KnownForFragment fragment = new KnownForFragment();
        Bundle args = new Bundle();
        args.putSerializable(KEY_PRODUCTS, (Serializable) products);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        activity = getActivity();
        if (getArguments() != null) {
            products = (List<ProductKnownFor>) getArguments().getSerializable(KEY_PRODUCTS);
        } else {
           // re-gain
            Utils.showConnectionDialog(activity);

        }
    }


    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        if (view == null) view = inflater.inflate(R.layout.fragment_known_for, container, false);


        init(view);
        viewModel = new ViewModelGlobal(activity);
        if (!isNetworkAvailable()) Utils.isRoomDataNeeded = true;

        if (products != null && products.size() >0)
            setMovieRecView(products);
        else
            Utils.showConnectionDialog(activity);


        return view;
    }

    // ---------------------------      Methods     --------------------------------//
    private void init(View view) {
        recyclerView = view.findViewById(R.id.known_for_rv);
        progressBar =  view.findViewById(R.id.progress_circular);
        progressBar.setVisibility(View.VISIBLE);
    }



    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void setMovieRecView(List<ProductKnownFor> products) {
        adapter = new MovieAdapter(products);
        recyclerView.setLayoutManager(new GridLayoutManager(activity, 1));
        recyclerView.setAdapter(adapter);
        progressBar.setVisibility(View.INVISIBLE);
    }


}