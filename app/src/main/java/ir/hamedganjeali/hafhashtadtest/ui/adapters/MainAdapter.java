package ir.hamedganjeali.hafhashtadtest.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import ir.hamedganjeali.hafhashtadtest.R;
import ir.hamedganjeali.hafhashtadtest.Utils;
import ir.hamedganjeali.hafhashtadtest.model.ActorWork;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ActorWorkViewHolder> {
    String picUrl ;
    List<ActorWork> actorWorks;
    OnMyClickListener listener;
    private int widthSplit = 0;

    public void setItemsInRow(int itemsCount) {
        this.widthSplit = itemsCount ;
    }

    public MainAdapter(List<ActorWork> ActorWorks, OnMyClickListener listener) {
        this.actorWorks = ActorWorks;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ActorWorkViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item, viewGroup, false);
        //screen_based width
        if (widthSplit > 0) {
            GridLayoutManager.LayoutParams layoutParams = (GridLayoutManager.LayoutParams) view.getLayoutParams();
            int split = (viewGroup.getMeasuredWidth() / widthSplit);
            layoutParams.width = split - split / 8;
            layoutParams.height = 5 * (layoutParams.width) / 3;
        }
        return new ActorWorkViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ActorWorkViewHolder holder, int i) {
        if (actorWorks.size() <1) return;
        
        ActorWork actorWork = actorWorks.get(i);
        picUrl = Utils.PHOTO_BASE_URL + actorWork.getProfilePath();

        String name = actorWork.getName();

        if (name == null || name.trim().equals("")) {
            holder.txtTitle.setText(R.string.no_name);
        } else  holder.txtTitle.setText(actorWork.getName());

        holder.imageView.setBackgroundResource(R.drawable.avatar);
        if (picUrl != null && picUrl.trim().length()>2)  Picasso.get().load(picUrl).into(holder.imageView);

        holder.imageView.setOnClickListener(v -> {
            listener.onClick(actorWork);
        });
    }

    @Override
    public int getItemCount() {
        return actorWorks.size();
    }


    public class ActorWorkViewHolder extends RecyclerView.ViewHolder {
        TextView txtTitle;
        ImageView imageView;

        public ActorWorkViewHolder(@NonNull View itemView) {
            super(itemView);
            txtTitle = itemView.findViewById(R.id.title);
            imageView = itemView.findViewById(R.id.image);
        }
    }


    public interface OnMyClickListener {
        void onClick(ActorWork selectedActorWork);
    }

}
