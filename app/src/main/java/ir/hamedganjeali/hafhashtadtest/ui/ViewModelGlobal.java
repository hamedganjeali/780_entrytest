package ir.hamedganjeali.hafhashtadtest.ui;


import android.app.Activity;
import android.content.Context;
import android.util.Log;

import java.util.List;

import androidx.lifecycle.LiveData;
import ir.hamedganjeali.hafhashtadtest.data.RepositoryGlobal;
import ir.hamedganjeali.hafhashtadtest.data.room.MyActorDatabase;
import ir.hamedganjeali.hafhashtadtest.data.sharedpreferences.SharedPrefManager;
import ir.hamedganjeali.hafhashtadtest.model.Actor;
import ir.hamedganjeali.hafhashtadtest.model.ActorWork;
import ir.hamedganjeali.hafhashtadtest.model.ProductKnownFor;

public class ViewModelGlobal {
    Activity activity;
    private RepositoryGlobal globalRepository;

    public ViewModelGlobal(Activity activity) {
        this.activity = activity;
    }


    // ------------------------------------------------------------//

    // receive
    public LiveData<Actor> getActorById(String actorSearchAddedUrl, String actorId, boolean isRoomDataNeeded) {
        if (isRoomDataNeeded){
            Log.i("TAG", "ViewModel Room"  );
            return MyActorDatabase.getInstance(activity).ActorDao().getActorById(actorId);
        }
        globalRepository = new RepositoryGlobal(activity, isRoomDataNeeded);{
            Log.i("TAG", "ViewModel Retrofit"  );
            return globalRepository.getActorDetailById(actorSearchAddedUrl, actorId);
        }
    }

    public LiveData<List<ProductKnownFor>> getAllProductsByActorId(String actorId) {
        return MyActorDatabase.getInstance(activity).ProductsKnownForDao().getAllProductsKnownForById(actorId);
    }

    /**
     * it directly inserts all data to Room
     * NOTE: you need to add Products to these ActorWorks manually , if fetching from Room
     */
    public LiveData<List<ActorWork>> getAllActorsWorks(String url, boolean isRoomDataNeeded, int currentPage) {
        if (isRoomDataNeeded){
            Log.i("TAG", "ViewModel => Room"  );
            return MyActorDatabase.getInstance(activity).ActorWorksDao().getActorWorks(currentPage);
        }
        else {
            Log.i("TAG", "ViewModel => Retrofit"  );
            globalRepository = new RepositoryGlobal(activity, isRoomDataNeeded);
            return globalRepository.getActorWorks(url);
        }

    }


    // ------------------------------------------------------------//
    public void saveLastDownloadedPage(String lastCallDay) {
        SharedPrefManager.getInstance(activity).setLastCall(lastCallDay);
    }

    public String getLastCallDay() {
        return SharedPrefManager.getInstance(activity).getLastCall();
    }

    public void saveLastDownloadedPage(int lastPage) {
        Log.i("TAG", "save LastDownloadedPage: " + lastPage);
        SharedPrefManager.getInstance(activity).setLastPage(lastPage);
    }

    public int getLastDownloadedPage() {
        Log.i("TAG", "get LastDownloadedPage: " + SharedPrefManager.getInstance(activity).getLastPage());

        return SharedPrefManager.getInstance(activity).getLastPage();
    }


    public void removeLastDownloadedPage() {
        Log.i("TAG", "remove LastDownloadedPage: called");
        SharedPrefManager.getInstance(activity).removeLastDownloadedPage();

    }

    public void clearSharedPref() {
        SharedPrefManager.getInstance(activity).clear();

    }
}
