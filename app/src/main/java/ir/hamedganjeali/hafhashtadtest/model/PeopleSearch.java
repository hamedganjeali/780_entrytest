
package ir.hamedganjeali.hafhashtadtest.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class PeopleSearch {

    @SerializedName("results")
    private List<ActorWork> actorWorks;

    @SerializedName("page")
    private int currentPage;

    @SerializedName("total_pages")
    private int totalPages;


    public List<ActorWork> getActorWorks() {
        return actorWorks;
    }

    public void setActorWorks(List<ActorWork> actorWorks) {
        this.actorWorks = actorWorks;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }
}
