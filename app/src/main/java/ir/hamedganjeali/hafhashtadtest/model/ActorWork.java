
package ir.hamedganjeali.hafhashtadtest.model;



import java.io.Serializable;
import java.util.Calendar;
import java.util.List;
import com.google.gson.annotations.SerializedName;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "actorWorks")
public class ActorWork implements Serializable {


    @PrimaryKey
    @SerializedName("id")
    @ColumnInfo(name = "id")
    protected Long actorId;

    @ColumnInfo(name = "page")
    private int page;

    @ColumnInfo(name = "total_pages")
    private int totalPages;


    @SerializedName("name")
    @ColumnInfo(name = "name")
    protected String name;

    @SerializedName("known_for_department") //e.x. "acting"
    @ColumnInfo(name = "known_for_department")
    protected String knownFor;

    @SerializedName("known_for")// movies that he is the star of it
    @Ignore
    protected List<ProductKnownFor> productKnownFor;

    @SerializedName("profile_path") // picture
    @ColumnInfo(name = "profile_path")
    protected String profilePath;


    @SerializedName("popularity")
    @ColumnInfo(name = "popularity")
    protected double popularity;
    //-----------------------------------------//


    public Long getActorId() {
        return actorId;
    }

    public void setActorId(Long actorId) {
        this.actorId = actorId;
    }

    public List<ProductKnownFor> getProductKnownFor() {
        return productKnownFor;
    }

    public void setProductKnownFor(List<ProductKnownFor> productKnownFor) {
        this.productKnownFor = productKnownFor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfilePath() {
        return profilePath;
    }

    public void setProfilePath(String profilePath) {
        this.profilePath = profilePath;
    }

    public String getKnownFor() {
        return knownFor;
    }

    public void setKnownFor(String knownFor) {
        this.knownFor = knownFor;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public double getPopularity() {
        return popularity;
    }

    public void setPopularity(double popularity) {
        this.popularity = popularity;
    }
}

