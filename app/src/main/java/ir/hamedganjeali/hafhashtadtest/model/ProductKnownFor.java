package ir.hamedganjeali.hafhashtadtest.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
@Entity(tableName = "product_Known_for")
public class ProductKnownFor implements Serializable {

    @PrimaryKey
    @SerializedName("id")
    @ColumnInfo(name = "id")
    protected Long productKnownForId;

    @SerializedName("poster_path")
    @ColumnInfo(name = "poster_path")
    protected String posterPath;

    @SerializedName("title")
    @ColumnInfo(name = "title")
    protected String title;

    @ColumnInfo(name = "actorId")
    protected Long actorId;

//------------------------------------------------//


    public Long getActorId() {
        return actorId;
    }

    public void setActorId(Long actorId) {
        this.actorId = actorId;
    }

    public Long getProductKnownForId() {
        return productKnownForId;
    }

    public void setProductKnownForId(Long productKnownForId) {
        this.productKnownForId = productKnownForId;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


}
