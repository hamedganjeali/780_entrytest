
package ir.hamedganjeali.hafhashtadtest.model;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "actor")
public class Actor implements Serializable {


    @PrimaryKey
    @SerializedName("id")
    @ColumnInfo(name = "id")
    protected Long actorId;


    @SerializedName("biography")
    @ColumnInfo(name = "biography")
    protected String biography;

    @SerializedName("birthday")
    @ColumnInfo(name = "birthday")
    protected String birthday;

    @SerializedName("deathday")
    @ColumnInfo(name = "deathday")
    protected String deathday;

    @SerializedName("gender")
    @ColumnInfo(name = "gender")
    protected Long gender;

    @SerializedName("known_for_department") //e.x. "acting"
    @ColumnInfo(name = "known_for_department")
    protected String knownFor;

    @SerializedName("also_known_as") // names in other languages
    @Ignore // room cannot add List
    protected List<String> asoKnownAs;

    @SerializedName("name")
    @ColumnInfo(name = "name")
    protected String name;

    @SerializedName("place_of_birth")
    @ColumnInfo(name = "place_of_birth")
    protected String placeOfBirth;

    @SerializedName("popularity")
    @ColumnInfo(name = "popularity")
    protected Double popularity;

    @SerializedName("profile_path") // picture
    @ColumnInfo(name = "profile_path")
    protected String profilePath;

    //-----------------------------------------//


    public Long getActorId() {
        return actorId;
    }

    public void setActorId(Long actorId) {
        this.actorId = actorId;
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getDeathday() {
        return deathday;
    }

    public void setDeathday(String deathday) {
        this.deathday = deathday;
    }

    public Long getGender() {
        return gender;
    }

    public void setGender(Long gender) {
        this.gender = gender;
    }

    public String getKnownFor() {
        return knownFor;
    }

    public void setKnownFor(String knownFor) {
        this.knownFor = knownFor;
    }

    public List<String> getAsoKnownAs() {
        return asoKnownAs;
    }

    public void setAsoKnownAs(List<String> asoKnownAs) {
        this.asoKnownAs = asoKnownAs;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    public Double getPopularity() {
        return popularity;
    }

    public void setPopularity(Double popularity) {
        this.popularity = popularity;
    }

    public String getProfilePath() {
        return profilePath;
    }

    public void setProfilePath(String profilePath) {
        this.profilePath = profilePath;
    }

    public String getAge() {
        if (getBirthday() ==null)
            return null;

        int bornYear = Integer.parseInt(getBirthday().substring(0, 4).trim()) ;
        int now = Calendar.getInstance().get(Calendar.YEAR);
        return String.valueOf(now - bornYear);
    }

    //-----------------------------------------//


}
