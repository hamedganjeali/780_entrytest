package ir.hamedganjeali.hafhashtadtest.data.room;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;
import ir.hamedganjeali.hafhashtadtest.model.Actor;

@Dao
public interface ActorDao {
    @Insert (onConflict = OnConflictStrategy.REPLACE)
    void insert(Actor actor);

    @Insert (onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Actor> actors);

    @Update
    void update(Actor actor);

    @Delete
    void delete(Actor actor);

    @Query("SELECT * FROM actor")
    List<Actor> getAllActors();

    @Query("SELECT* FROM actor WHERE id =  :arg0")
   LiveData<Actor> getActorById(String arg0);


}
