package ir.hamedganjeali.hafhashtadtest.data.room;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;
import ir.hamedganjeali.hafhashtadtest.model.ActorWork;

@Dao
public interface ActorWorksDao {
    @Insert (onConflict = OnConflictStrategy.REPLACE)
    void insert(ActorWork actorWorks);

    @Insert (onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<ActorWork> actorWorks);

    @Update
    void update(ActorWork actorWorks);

    @Delete
    void delete(ActorWork actorWorks);

    @Query("SELECT * FROM actorWorks WHERE page = :page ORDER BY popularity DESC")
    LiveData<List<ActorWork>> getActorWorks(int page);

    @Query("SELECT* FROM actorWorks WHERE id =  :arg0")
    LiveData<ActorWork> getActorWorksById(Long arg0);



}
