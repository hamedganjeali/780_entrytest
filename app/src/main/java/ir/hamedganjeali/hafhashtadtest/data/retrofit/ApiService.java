package ir.hamedganjeali.hafhashtadtest.data.retrofit;



import ir.hamedganjeali.hafhashtadtest.Utils;
import ir.hamedganjeali.hafhashtadtest.model.Actor;
import ir.hamedganjeali.hafhashtadtest.model.PeopleSearch;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface ApiService {


    //-------------------   Main    -------------------//
    @GET()
    Call<PeopleSearch> getPeopleSearch(@Url String url);


    //-------------------   Detail    -------------------//
    @GET()
    Call<Actor> getActorDetails(@Url String url, @Query("id") String actorId);



}
