package ir.hamedganjeali.hafhashtadtest.data.room;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;
import ir.hamedganjeali.hafhashtadtest.model.ProductKnownFor;

@Dao
public interface ProductsKnownForDao {
    @Insert (onConflict = OnConflictStrategy.REPLACE)
    void insert(ProductKnownFor product);

    @Insert (onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<ProductKnownFor> products);


    @Update
    void update(ProductKnownFor product);

    @Delete
    void delete(ProductKnownFor product);

    @Query("SELECT * FROM product_Known_for  WHERE actorId = :actorId")
    LiveData<List<ProductKnownFor>> getAllProductsKnownForById(String actorId);



}
