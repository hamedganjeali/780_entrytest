package ir.hamedganjeali.hafhashtadtest.data;

import android.content.Context;

import java.util.List;

import androidx.lifecycle.MutableLiveData;
import ir.hamedganjeali.hafhashtadtest.model.Actor;
import ir.hamedganjeali.hafhashtadtest.model.ActorWork;
import retrofit2.http.Url;

public interface DataSourceGlobal {



    MutableLiveData<Actor> getActorDetailById(String actorSearchAddedUrl, String actorId);
    MutableLiveData<List<ActorWork>> getActorWorks(String url); // only id , movies , and photo url
}
