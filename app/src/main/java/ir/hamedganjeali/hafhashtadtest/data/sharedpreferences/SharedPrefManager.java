package ir.hamedganjeali.hafhashtadtest.data.sharedpreferences;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class SharedPrefManager {

    private static final String KEY_DAY_OF_CALL = "day_of_call";
    private static final String SHARED_PREF_NAME = "day_of_call_shared_preferences";
    private static final String KEY_LAST_PAGE_CALL = "last_page_call";

    @SuppressLint("StaticFieldLeak")
    private static SharedPrefManager mInstance;
    private final Context mContext;
    private SharedPreferences sharedPreferences;

    private SharedPrefManager(Context context) {
        mContext = context;
        sharedPreferences = mContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
    }


    public static synchronized SharedPrefManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharedPrefManager(context);
        }
        return mInstance;
    }


    //------------------------   Methods  --------------------------//

    public void setLastCall(String lastCallDay) {
        sharedPreferences = mContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(KEY_DAY_OF_CALL, lastCallDay); // def can even be null. gives no problems
            editor.apply();

    }


    public String getLastCall() {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_DAY_OF_CALL, "once"); // def can even be null. gives no problems
    }


    public void setLastPage(int lastPage) {
        sharedPreferences = mContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(KEY_LAST_PAGE_CALL, lastPage);
        editor.apply();

    }


    public int getLastPage() {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getInt(KEY_LAST_PAGE_CALL, 0);
    }

    public void removeLastDownloadedPage() {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(KEY_LAST_PAGE_CALL);
        editor.apply();
    }

    public void clear() {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }

}
