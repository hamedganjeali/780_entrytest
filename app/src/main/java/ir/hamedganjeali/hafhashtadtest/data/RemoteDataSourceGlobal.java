package ir.hamedganjeali.hafhashtadtest.data;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

import androidx.lifecycle.MutableLiveData;
import ir.hamedganjeali.hafhashtadtest.Utils;
import ir.hamedganjeali.hafhashtadtest.data.room.MyActorDatabase;
import ir.hamedganjeali.hafhashtadtest.model.Actor;
import ir.hamedganjeali.hafhashtadtest.data.retrofit.ApiProvider;
import ir.hamedganjeali.hafhashtadtest.data.retrofit.ApiService;
import ir.hamedganjeali.hafhashtadtest.model.ActorWork;
import ir.hamedganjeali.hafhashtadtest.model.PeopleSearch;
import ir.hamedganjeali.hafhashtadtest.model.ProductKnownFor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RemoteDataSourceGlobal implements DataSourceGlobal {
    private ApiService apiService = ApiProvider.getApiServiceInstance();
    Activity activity;
    boolean isRoomDataNeeded;

    public RemoteDataSourceGlobal(Activity activity, boolean isRoomDataNeeded) {
        this.activity = activity;
        this.isRoomDataNeeded = isRoomDataNeeded;
    }


    @Override
    public MutableLiveData<Actor> getActorDetailById(String actorSearchAddedUrl, String actorId) {
        final MutableLiveData<Actor> actor = new MutableLiveData<>();

        apiService.getActorDetails(actorSearchAddedUrl, actorId).enqueue(new Callback<Actor>() {

            @Override
            public void onResponse(Call<Actor> call, Response<Actor> response) {
                // add to actor to Room directly
                new Thread() {
                    @Override
                    public void run() {
                        MyActorDatabase.getInstance(activity).ActorDao().insert(response.body());
                    }
                }.start();

                actor.setValue(response.body());

            }

            @Override
            public void onFailure(Call<Actor> call, Throwable t) {
                Toast.makeText(activity, "Error: " + t.getMessage(), Toast.LENGTH_SHORT).show();
                Utils.showConnectionDialog(activity);
//                Log.i("LOG", "DetailRemoteDataSource > getActorDetailById - onFailure: " + t.toString());
            }
        });

        return actor;
    }


    @Override
    public MutableLiveData<List<ActorWork>> getActorWorks(String url) {
        final MutableLiveData<List<ActorWork>> liveData = new MutableLiveData<>();
        apiService.getPeopleSearch(url).enqueue(new Callback<PeopleSearch>() {
            @Override
            public void onResponse(Call<PeopleSearch> call, Response<PeopleSearch> response) {

                PeopleSearch peopleSearch = response.body();
                List<ActorWork> actorWorks = peopleSearch.getActorWorks();

                int totalPages = peopleSearch.getTotalPages();
                int currentPage = peopleSearch.getCurrentPage();
                for (ActorWork actorWork : actorWorks) {
                    actorWork.setTotalPages(totalPages);
                    actorWork.setPage(currentPage);
                }


                // add to Room
                new Thread() {
                    @Override
                    public void run() {
                        // add to products of each to Room directly
                        List<ProductKnownFor> products;
                        for (ActorWork actorWork : actorWorks) {
                            actorWork.setTotalPages(totalPages);
                            actorWork.setPage(currentPage);
                            // set actorId to each product
                            products = actorWork.getProductKnownFor();
                            for (ProductKnownFor product : products) {
                                product.setActorId(actorWork.getActorId());
                            }
                            MyActorDatabase.getInstance(activity).ProductsKnownForDao().insertAll(products);
                        }
                        // add to actorWorks Room directly
                        MyActorDatabase.getInstance(activity).ActorWorksDao().insertAll(actorWorks);

                    }
                }.start();

                liveData.setValue(actorWorks);

            }

            @Override
            public void onFailure(Call<PeopleSearch> call, Throwable t) {
                Toast.makeText(activity, "Error: " + t.getMessage(), Toast.LENGTH_SHORT).show();
                Utils.showConnectionDialog(activity);
//                Log.i("TAG", "RemoteDataSourceGlobal - onFailure called: " + t.toString());

            }
        });


        return liveData;
    }


}

