package ir.hamedganjeali.hafhashtadtest.data.retrofit;

public class ApiProvider {
    public static ApiService apiService;

    public static ApiService getApiServiceInstance(){
        if(apiService==null){
            apiService=ApiClient.getClient().create(ApiService.class);
        }
        return apiService;
    }
}