package ir.hamedganjeali.hafhashtadtest.data.room;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import ir.hamedganjeali.hafhashtadtest.model.Actor;
import ir.hamedganjeali.hafhashtadtest.model.ActorWork;
import ir.hamedganjeali.hafhashtadtest.model.ProductKnownFor;


@Database(entities = {Actor.class, ActorWork.class, ProductKnownFor.class}, version = 1, exportSchema = false)
public abstract class MyActorDatabase extends RoomDatabase {

    private static MyActorDatabase instance;

    public abstract ActorDao ActorDao();
    public abstract ActorWorksDao ActorWorksDao();
    public abstract ProductsKnownForDao ProductsKnownForDao();

    public static synchronized MyActorDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context, MyActorDatabase.class, "actors_db").build();

        }
        return instance;
    }


}
