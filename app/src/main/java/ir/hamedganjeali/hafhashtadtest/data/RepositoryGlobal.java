package ir.hamedganjeali.hafhashtadtest.data;

import android.app.Activity;
import android.content.Context;

import java.util.List;

import androidx.lifecycle.MutableLiveData;
import ir.hamedganjeali.hafhashtadtest.model.Actor;
import ir.hamedganjeali.hafhashtadtest.model.ActorWork;


public class RepositoryGlobal implements DataSourceGlobal {
    Activity activity;
    boolean isRoomDataNeeded;
    RemoteDataSourceGlobal remoteDS;

    public RepositoryGlobal(Activity activity, boolean isRoomDataNeeded) {
        this.activity = activity;
        this.isRoomDataNeeded = isRoomDataNeeded;
        remoteDS = new RemoteDataSourceGlobal(activity, isRoomDataNeeded);
    }

    @Override
    public MutableLiveData<Actor> getActorDetailById(String actorSearchAddedUrl, String actorId) {
        return remoteDS.getActorDetailById( actorSearchAddedUrl, actorId);
    }

    @Override
    public MutableLiveData<List<ActorWork>> getActorWorks(String url) {
        return remoteDS.getActorWorks(url);
    }

}
