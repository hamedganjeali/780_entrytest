package ir.hamedganjeali.hafhashtadtest;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatButton;
import ir.hamedganjeali.hafhashtadtest.ui.MainActivity;
import ir.hamedganjeali.hafhashtadtest.ui.ViewModelGlobal;

public class Utils {
    public static final String BASE_URL = "https://api.themoviedb.org/";
    public static final String PEOPLE_SEARCH_ADDED_URL = "3/person/";
    public static final String POPULAR_ACTORS_ADDED_URL = "3/person/popular";
    public static final String PHOTO_BASE_URL = "https://image.tmdb.org/t/p/w185/";
    public static final String API_KEY = "api_key";
    public static final String API_KEY_VALUE = "329afbbfe5eff717039cf8abf0fef2db";

    public static final String KEY_ACTOR_WORK_OBJ = "key_actor_object" ;

    public static boolean isRoomDataNeeded = false;



    public static void showConnectionDialog(Activity activity) {
        new ViewModelGlobal(activity).clearSharedPref();

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_warning);
        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;


        ((AppCompatButton) dialog.findViewById(R.id.bt_close)).setOnClickListener((View.OnClickListener) v -> {
            activity.startActivity(new Intent(activity, MainActivity.class));
            activity.finish();
            dialog.dismiss();
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }
}

// e.x. of profile path : /gThaIXgpCm3PCiXwFNDBJCme85y.jpg
// e.x. of poster path : /x2LSRK2Cm7MZhjluni1msVJ3wDF.jpg